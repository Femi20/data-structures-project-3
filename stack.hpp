template <typename T>
Stack<T>::Stack(): theSize{0}//zero-argument constructor. 
{}

template <typename T>
Stack<T>::~Stack()	// destructor 
{
	s.clear();
	theSize = 0;
}

template <typename T>
Stack<T>::Stack (const Stack<T>& rhs): theSize{rhs.theSize} //copy constructor.
{
		for(auto c: rhs.s)
			push(c);
		
}

template <typename T>
Stack<T>::Stack(Stack<T> && rhs): theSize{rhs.theSize} //	move constructor.
{
	for(auto x: rhs)
		push(x);
	
	rhs.clear();
	rhs.theSize = 0;
}

template <typename T>
const Stack<T>& Stack<T>::operator= (const Stack <T>& rhs) //copy assignment operator=.
{
		for(auto x: rhs.s)
			push(x);
		
	//std::swap(theSize, rhs.theSize);

	
	return *this;
}

template <typename T>
Stack<T> & Stack<T>::operator=(Stack<T> && rhs) //move assignment operator=
{
	for(auto x: rhs)
		push(x);
	
	theSize = std::move(rhs.theSize);

	
	return *this;
}

template <typename T>
void Stack<T>::print(std::ostream& os, char ofc) const
{
	std::list<T>temp = s;
	temp.reverse();
	
		for(auto i : temp)
			os << i << ofc;
}

template <typename T>
bool Stack<T>::empty() const //returns true if the Stack contains no elements, and false otherwise.  
{
	return s.empty();
}

template <typename T>
void Stack<T>::clear() //delete all elements from the stack.
{
	s.clear();
}

template <typename T>
void Stack<T>::push(const T& x)//adds  x  to the Stack.   copy version.
{
	s.push_front(x);

}


template <typename T>
void Stack<T>::push(T && x) //adds x to the Stack. move version.
{
	s.push_front(std::move(x));
}


template <typename T>
void Stack<T>::pop() //removes and discards the most recently added element of the Stack.  
{
	s.pop_front();
	
}


template <typename T>
T& Stack<T>::top() //mutator that returns a reference to the most recently added element of the Stack.  
{
	return s.front();
}


template <typename T>
const T& Stack<T>::top() const //accessor that returns the most recently added element of the Stack.  
{
	return s.front();
}				

template <typename T>
int Stack<T>::size() const //returns the number of elements stored in the Stack
{
	return s.size();
}


//overloading output operator
template <typename T>
std::ostream& operator<< (std::ostream& os, const Stack<T>& a) //invokes the print() method to print the Stack<T> a in the specified ostream    
{
	a.print(os);
	
	return os;
}

//Overloading comparison operators	
template <typename T>
bool operator== (const Stack<T>& lhs, const Stack <T>& rhs) //returns true if the two compared Stacks have the same elements, in the same order.  
{
	Stack<T> l = lhs;
	Stack<T> r = rhs;
	
	if(l.size() != r.size()) return false;
	
	while(!l.empty() && !r.empty())
	{
		if(l.top() != r.top())
		{
			return false;
		}
		
		l.pop();
		r.pop();
	}
	
	return true;
}
	
	
template <typename T>
bool operator!= (const Stack<T>& lhs , const Stack <T>& rhs) //opposite of operator==()
{
	return !(lhs == rhs);
	
}

template <typename T>
bool operator<= (const Stack<T>& a, const Stack <T>& b)
{
	
	Stack<T> l = a;
	Stack<T> r = b;
	
	while(!l.empty())
	{
		if(l.top() > r.top())
		{
			return false;
		}
		
		l.pop();
		r.pop();
	}
	
	return true;
}