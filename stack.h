#ifndef STACK_H
#define STACK_H
#include <iostream>
#include <list>
#include <iterator>
namespace cop4530 {


template <typename T>
	class Stack {
		
			private:
				std::list<T> s;
				int theSize;
				
				
			public:
				
				// constructor, destructor, copy constructor
				Stack(); 	//zero-argument constructor.   
				~Stack ();	// destructor  
				Stack (const Stack<T>&); //copy constructor.   
				Stack(Stack<T> &&); //	move constructor.

				const Stack& operator= (const Stack <T>&); //copy assignment operator=.
			
				Stack<T> & operator=(Stack<T> &&); //move assignment operator=

				//Print elements of stack to ostream os
				void print(std::ostream& os, char ofc = ' ') const;
				
				//Member functions
				bool empty() const; //returns true if the Stack contains no elements, and false otherwise.  
				void clear(); //delete all elements from the stack.
				void push(const T& x); //adds  x  to the Stack.   copy version.
				void push(T && x); //adds x to the Stack. move version.
				void pop(); //removes and discards the most recently added element of the Stack.  
				T& top(); //mutator that returns a reference to the most recently added element of the Stack.  
				const T& top() const; //accessor that returns the most recently added element of the Stack.  
				int size() const; //returns the number of elements stored in the Stack.
	};
	
	//overloading output operator
	template <typename T>
		std::ostream& operator<< (std::ostream& os, const Stack<T>& a); //invokes the print() method to print the Stack<T> a in the specified ostream    

	//Overloading comparison operators	
	template <typename T>
		bool operator== (const Stack<T>& lhs, const Stack <T>& rhs); //returns true if the two compared Stacks have the same elements, in the same order.  

	template <typename T>
		bool operator!= (const Stack<T>& lhs, const Stack <T>& rhs); //opposite of operator==().
	
	template <typename T>
		bool operator<= (const Stack<T>& a, const Stack <T>& b);
	
	
	#include "stack.hpp"
	
}//end of namespace 4530

#endif
	
