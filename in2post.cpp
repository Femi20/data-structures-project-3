#include "stack.h"
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip>
#include <algorithm>
using namespace std;


int pre(string p)
{
	if(p == "+" || p == "-")
			return 1;
	
	else if(p == "*" || p == "/")
			return 2;
	
	else if(p == "(")
			return 3;
		
	else 	
		return 0;
}


bool isOp(const string & s)
{
	char c;
	for(int i = 0; i < s.size(); i++)
	{
		c = s[i];
		
		if(!isalnum(c) && c != '.' && c != '_')
			return false;
	}
	
	return true;
}

bool isNum(const string & s)
{
	if (s.empty())
		return false;
	
	for(auto c : s)
	{
		if(!(isdigit(c) || c == '.') || isalpha(c))
			return false;
		
	}
	return true;
		
}
	
bool isMatch(string c1, string c2)
{
	return (c1 == "(" && c2 == ")");
}

bool checkParenthesis(vector <string> exp)
{
	cop4530::Stack <string> check;
	
	for(int i = 0; i < exp.size(); i++)
	{
		if(exp[i] == "(")
		{
			check.push(exp[i]);
		}
		
		if(exp[i] == ")")
		{
			if(check.empty())
				return false;
			
			
			else if(isMatch(check.top(), exp[i]))
			{
				check.pop();
			}
		}		
		
	}
	
	return true;
}
	
void in2post(string &expression, vector<string>& res, bool &flag)
{
	stringstream err(expression);
	vector <string> check;
	string t;
	while(err >> t)
	{
		check.push_back(t);
	}
	
	if(checkParenthesis(check) == false)
	{
		cout << "Error: Mismatched Parenthesis!\n";
			flag = false;
			return;
	}
	if(!isOp(check[check.size()-1]) && check[check.size()-1] != ")")
	{
		cout << "Error: Invalid expression!\n";
			flag = false;
			return;
	}
	
	if(!isOp(check[0]) && !isNum(check[0]) && check[0] != "(")
	{
		cout << "Error: Invalid expression!\n";
			flag = false;
			return;
	}
	
	for(int i=0; i < check.size(); i++)
	{
		if(check[i] == ")" && isOp(check[i-1]) == false && check[i-1] != ")")
		{
			
			cout << "Error: Incomplete expression in parenthesis!\n";
			flag = false;
			return;
		}
		
		if(i != 0 && check[i] == "(" && isOp(check[i-1]))
		{
			cout << " Error: Invalid expression!\n";
			flag = false;
			return;
		}
	}

	stringstream ss(expression);
	string tok;
	
	cop4530::Stack <string> postStack;
	
	while(ss >> tok)
	{
		
		
		if(isOp(tok))
		{
			res.push_back(tok);
		}
		
		else if(tok == "+" || tok == "-" || tok == "/" || tok == "*" || tok == "(")
		{
			
			while(!postStack.empty() && postStack.top() != "(" && pre(postStack.top()) >= pre(tok) )
			{
				res.push_back(postStack.top());
				postStack.pop();
			}
		
			postStack.push(tok);
		
		}
		
		
		else if(tok == ")")
		{
			
			while(postStack.top() != "("){
				res.push_back(postStack.top());
				postStack.pop();
				
				if(postStack.empty())
					break;
			}
			
			postStack.pop(); 
		
		}
		
	}
	
	if(postStack.empty())
	{
		flag  = false;
		cout << "Error: Invalid Expression!\n";
		return;
	}
	
	flag = true;		
	while (!postStack.empty())
	{
		res.push_back(postStack.top());
		postStack.pop();
	}
}


double postEval (const vector<string> &postFix, bool flag)
{
	if(flag == false)
	{
		cout << "Cannot evalute invalid expression\n";
		return 0;
	}
	
	cop4530::Stack<double> eval;
	double op1;
	double op2;
	
	string::size_type s;
	
	for(int i = 0; i < postFix.size(); i++)
	{
		if(isNum(postFix[i]))
		{
			eval.push(stod(postFix[i]));
		}
			
		if(postFix[i] == "+")
		{
			
			op1 = eval.top();
			eval.pop();
			op2 = eval.top();
			eval.pop();
			eval.push(op1 + op2);
		}
		
		else if( postFix[i] == "-")
		{
			op1 = eval.top();
			eval.pop();
			op2 = eval.top();
			eval.pop();
			eval.push(op2 - op1);
		}
		
		else if( postFix[i] == "*")
		{
			op1 = eval.top();
			eval.pop();
			op2 = eval.top();
			eval.pop();
			eval.push(op1 * op2);
		}
		
		else if( postFix[i] == "/")
		{
			op1 = eval.top();
			eval.pop();
			op2 = eval.top();
			eval.pop();
			eval.push(op2 / op1);
		}
		
		else if(!isNum(postFix[i]))
		{
			cout << "Postfix Evaluation: ";
			for (auto c : postFix)
				cout << c << " ";
			
			cout << "= ";
			
			for (auto c : postFix)
				cout << c << " ";
			
			cout << endl;
			return 0;
		}
		
		
	}
	
	cout << "Postfix Evaluation: ";
	for (auto c : postFix)
				cout << c << " ";

	cout << "= " << eval.top() << endl;
		
	
	return eval.top();
}


int main()
{
	string exp;
	vector <string> res;
	double answer;
	bool complete;
	
	cout << "Enter infix expression (\"exit\" to quit): ";
	
	
	while(getline(cin, exp) && exp != "exit")
	{		
		cout << "\n";
		
		in2post(exp, res, complete);
		
		
		if(complete == false)
			cout << "Expression: " << exp;
	
		else
		{
		
			cout << "Postfix Expression: ";
			
			for(auto i : res)
				cout << i << " ";
		}
		cout << endl;
		
		answer = postEval(res, complete);

		res.clear();
		
		cout << "\n";
		
		cout << "Enter infix expression (\"exit\" to quit): ";
	}
		
	return 0;
}
