### Objectives ###
			
			Understand the stack ADT and its applications.
			Understand infix to postfix conversion and postfix expression evaluation. 

### Statement of Work ###
			
			Implement a generic stack container as an adaptor class template. 
			Implement a program that converts infix expression to postfix expression.
			Implement a program that evaluates postfix expression using the stack container you develop.


### How do I get set up? ###

			To create executable: Type 'make proj3.x'
			To run: Type 'proj3.x'
			