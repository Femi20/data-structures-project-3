#CXX=g++47
CXXFLAGS=-std=c++11 -o

.SUFFIXES: .x

.PHONY: clean


clean:
	rm -f *.o *.x core.*
	
proj3.x: test_stack1.cpp stack.h stack.hpp
	$(CXX) $(CXXFLAGS) $@ $<

in2post.x: in2post.cpp stack.h stack.hpp
	$(CXX) $(CXXFLAGS) $@ $<	
